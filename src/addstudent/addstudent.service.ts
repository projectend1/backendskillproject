import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAddstudentDto } from './dto/create-addstudent.dto';
import { UpdateAddstudentDto } from './dto/update-addstudent.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Addstudent } from './entities/addstudent.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AddstudentService {
  constructor(
    @InjectRepository(Addstudent)
    private addstudentRepository: Repository<Addstudent>,
  ) {}

  create(createAddstudentDto: CreateAddstudentDto) {
    return this.addstudentRepository.save(createAddstudentDto);
  }

  findAll() {
    return this.addstudentRepository.find();
  }
  findAllByYear(year: string) {
    return this.addstudentRepository.find({ where: { year: year } });
  }

  findOne(id: number) {
    return this.addstudentRepository.findOne({ where: { id: id } });
  }

  findOneByYear(year: string) {
    return this.addstudentRepository.findOne({ where: { year: year } });
  }

  async update(id: number, updateAddstudentDto: UpdateAddstudentDto) {
    try {
      const updatedAddstudentDto = await this.addstudentRepository.save({
        id,
        ...updateAddstudentDto,
      });
      return updatedAddstudentDto;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const addstudent = await this.addstudentRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedAddstudent =
        await this.addstudentRepository.remove(addstudent);
      return deletedAddstudent;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
