import { IsNotEmpty } from 'class-validator';

export class CreateAddstudentDto {
  @IsNotEmpty()
  idstudent: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  lastname: string;

  @IsNotEmpty()
  year: string;
}
