import { PartialType } from '@nestjs/mapped-types';
import { CreateAddstudentDto } from './create-addstudent.dto';

export class UpdateAddstudentDto extends PartialType(CreateAddstudentDto) {}
