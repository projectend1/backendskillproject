import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Addstudent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  idstudent: string;

  @Column()
  name: string;

  @Column()
  lastname: string;

  @Column()
  year: string;
}
