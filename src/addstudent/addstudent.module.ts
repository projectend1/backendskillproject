import { Module } from '@nestjs/common';
import { AddstudentService } from './addstudent.service';
import { AddstudentController } from './addstudent.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Addstudent } from './entities/addstudent.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Addstudent])],
  controllers: [AddstudentController],
  providers: [AddstudentService],
})
export class AddstudentModule {}
