import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AddstudentService } from './addstudent.service';
import { CreateAddstudentDto } from './dto/create-addstudent.dto';
import { UpdateAddstudentDto } from './dto/update-addstudent.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('addstudent')
@UseGuards(JwtAuthGuard)
export class AddstudentController {
  constructor(private readonly addstudentService: AddstudentService) {}

  @Post()
  create(@Body() createAddstudentDto: CreateAddstudentDto) {
    return this.addstudentService.create(createAddstudentDto);
  }

  @Get()
  findAll() {
    return this.addstudentService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.addstudentService.findOne(+id);
  }
  @Get('/findAllByYear/:id')
  findAllByYear(@Param('id') year: string) {
    return this.addstudentService.findAllByYear(year);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAddstudentDto: UpdateAddstudentDto,
  ) {
    return this.addstudentService.update(+id, updateAddstudentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.addstudentService.remove(+id);
  }
}
