import { IsNumber, IsString } from 'class-validator';

export class CreateCertificateDto {
  @IsNumber()
  id: number;

  @IsString()
  @IsNumber()
  img: string;

  @IsString()
  description: string;

  @IsString()
  link: string;
}
