import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Certificate {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  img: string;

  @Column()
  description: string;

  @Column()
  link: string;
}
