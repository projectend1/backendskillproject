import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCertificateDto } from './dto/create-certificate.dto';
import { UpdateCertificateDto } from './dto/update-certificate.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Certificate } from './entities/certificate.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CertificatesService {
  constructor(
    @InjectRepository(Certificate)
    private certificateRepository: Repository<Certificate>,
  ) {}
  create(createCertificateDto: CreateCertificateDto) {
    return this.certificateRepository.save(createCertificateDto);
  }

  findAll() {
    return this.certificateRepository.find();
  }

  findOne(id: number) {
    return this.certificateRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateCertificateDto: UpdateCertificateDto) {
    try {
      const updatedCertificate = await this.certificateRepository.save({
        id,
        ...updateCertificateDto,
      });
      return updatedCertificate;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const certificate = await this.certificateRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedCertificateDto =
        await this.certificateRepository.softRemove(certificate);
      return deletedCertificateDto;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
