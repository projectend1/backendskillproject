import { IsNotEmpty } from 'class-validator';
export class CreateMainskillmappingDto {
  @IsNotEmpty()
  ploId: number;

  @IsNotEmpty()
  mainskillId: number;

  @IsNotEmpty()
  Level: number;

  @IsNotEmpty()
  subjectId: number;
}
