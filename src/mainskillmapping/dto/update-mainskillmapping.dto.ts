import { PartialType } from '@nestjs/mapped-types';
import { CreateMainskillmappingDto } from './create-mainskillmapping.dto';

export class UpdateMainskillmappingDto extends PartialType(
  CreateMainskillmappingDto,
) {}
