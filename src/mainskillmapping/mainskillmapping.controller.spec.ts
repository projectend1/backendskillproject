import { Test, TestingModule } from '@nestjs/testing';
import { MainskillmappingController } from './mainskillmapping.controller';
import { MainskillmappingService } from './mainskillmapping.service';

describe('MainskillmappingController', () => {
  let controller: MainskillmappingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MainskillmappingController],
      providers: [MainskillmappingService],
    }).compile();

    controller = module.get<MainskillmappingController>(
      MainskillmappingController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
