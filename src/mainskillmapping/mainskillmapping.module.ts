import { Module } from '@nestjs/common';
import { MainskillmappingService } from './mainskillmapping.service';
import { MainskillmappingController } from './mainskillmapping.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mainskillmapping } from './entities/mainskillmapping.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Mainskillmapping])],
  controllers: [MainskillmappingController],
  providers: [MainskillmappingService],
})
export class MainskillmappingModule {}
