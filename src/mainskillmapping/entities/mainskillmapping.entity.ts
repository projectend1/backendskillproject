import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Mainskillmapping {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ploId: number;

  @Column()
  mainskillId: number;

  @Column()
  Level: number;

  @Column()
  subjectId: number;
}
