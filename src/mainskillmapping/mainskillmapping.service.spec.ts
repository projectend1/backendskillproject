import { Test, TestingModule } from '@nestjs/testing';
import { MainskillmappingService } from './mainskillmapping.service';

describe('MainskillmappingService', () => {
  let service: MainskillmappingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MainskillmappingService],
    }).compile();

    service = module.get<MainskillmappingService>(MainskillmappingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
