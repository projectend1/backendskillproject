import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMainskillmappingDto } from './dto/create-mainskillmapping.dto';
import { UpdateMainskillmappingDto } from './dto/update-mainskillmapping.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Mainskillmapping } from './entities/mainskillmapping.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MainskillmappingService {
  constructor(
    @InjectRepository(Mainskillmapping)
    private mainskillmappingRepository: Repository<Mainskillmapping>,
  ) {}

  create(createMainskillmappingDto: CreateMainskillmappingDto) {
    return this.mainskillmappingRepository.save(createMainskillmappingDto);
  }

  findAll() {
    return this.mainskillmappingRepository.query(
      'SELECT msp.id, pl.PLO_id , sb.course , sb.nameEng , ms.namemainEng ,lv.level FROM mainskillmapping msp INNER JOIN plo pl ON msp.ploId = pl.Id INNER JOIN subject sb ON msp.subjectId = sb.Id INNER JOIN mainskill ms ON msp.mainskillId = ms.Id INNER JOIN level lv on msp.Level = lv.Id ',
    );
  }

  getbysubjectId(subjectId: number) {
    return this.mainskillmappingRepository.query(
      'SELECT DISTINCT msp.id, pl.PLO_id ,sb.course , sb.nameEng , ms.namemainEng ,lv.level FROM mainskillmapping msp INNER JOIN plo pl ON msp.ploId = pl.Id INNER JOIN subject sb ON msp.subjectId = sb.Id INNER JOIN mainskill ms ON msp.mainskillId = ms.Id INNER JOIN level lv on msp.Level = lv.Id WHERE sb.course = ?',
      [subjectId],
    );
  }

  findOne(id: number) {
    return this.mainskillmappingRepository.findOne({ where: { id: id } });
  }

  async update(
    id: number,
    updateMainskillmappingDto: UpdateMainskillmappingDto,
  ) {
    try {
      const updatedMainskillmapping =
        await this.mainskillmappingRepository.save({
          id,
          ...updateMainskillmappingDto,
        });
      return updatedMainskillmapping;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const mainskillmapping = await this.mainskillmappingRepository.findOne({
      where: { id: id },
    });
    try {
      const deleteddMainskillmapping =
        await this.mainskillmappingRepository.remove(mainskillmapping);
      return deleteddMainskillmapping;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
