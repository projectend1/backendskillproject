import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { MainskillmappingService } from './mainskillmapping.service';
import { CreateMainskillmappingDto } from './dto/create-mainskillmapping.dto';
import { UpdateMainskillmappingDto } from './dto/update-mainskillmapping.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('mainskillmapping')
@UseGuards(JwtAuthGuard)
export class MainskillmappingController {
  constructor(
    private readonly mainskillmappingService: MainskillmappingService,
  ) {}

  @Post()
  create(@Body() createMainskillmappingDto: CreateMainskillmappingDto) {
    return this.mainskillmappingService.create(createMainskillmappingDto);
  }

  @Get()
  findAll() {
    return this.mainskillmappingService.findAll();
  }

  @Get('/getbysubjectId/:id')
  getbysubjectId(@Param('id') subjectId: string) {
    return this.mainskillmappingService.getbysubjectId(+subjectId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.mainskillmappingService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMainskillmappingDto: UpdateMainskillmappingDto,
  ) {
    return this.mainskillmappingService.update(+id, updateMainskillmappingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.mainskillmappingService.remove(+id);
  }
}
