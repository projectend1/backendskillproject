import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePloDto } from './dto/create-plo.dto';
import { UpdatePloDto } from './dto/update-plo.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Plo } from './entities/plo.entity';

@Injectable()
export class PloService {
  constructor(
    @InjectRepository(Plo)
    private ploRepository: Repository<Plo>,
  ) {}

  create(createPloDto: CreatePloDto) {
    return this.ploRepository.save(createPloDto);
  }

  findAll() {
    return this.ploRepository.find();
  }

  findOne(id: number) {
    return this.ploRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updatePloDto: UpdatePloDto) {
    try {
      const updatedPlo = await this.ploRepository.save({
        id,
        ...updatePloDto,
      });
      return updatedPlo;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const plo = await this.ploRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedPlo = await this.ploRepository.remove(plo);
      return deletedPlo;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
