import { IsNotEmpty } from 'class-validator';
export class CreatePloDto {
  @IsNotEmpty()
  PLO_id: string;

  @IsNotEmpty()
  PLO_aptitude: string;

  @IsNotEmpty()
  PLO_result: string;
}
