import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Plo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  PLO_id: string;

  @Column()
  PLO_aptitude: string;

  @Column()
  PLO_result: string;
}
