import { Module } from '@nestjs/common';
import { PloService } from './plo.service';
import { PloController } from './plo.controller';
import { Plo } from './entities/plo.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Plo])],
  controllers: [PloController],
  providers: [PloService],
})
export class PloModule {}
