import { IsNotEmpty } from 'class-validator';

export class CreateSubjectDto {
  @IsNotEmpty()
  course: string;

  @IsNotEmpty()
  nameEng: string;

  @IsNotEmpty()
  nameTh: string;

  @IsNotEmpty()
  descriptionEng: string;

  @IsNotEmpty()
  descriptionTh: string;
}
