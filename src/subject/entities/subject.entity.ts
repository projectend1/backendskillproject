import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Subject {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  course: string;

  @Column()
  nameEng: string;

  @Column()
  nameTh: string;

  @Column()
  descriptionEng: string;

  @Column()
  descriptionTh: string;
}
