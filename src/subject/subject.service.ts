import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSubjectDto } from './dto/create-subject.dto';
import { UpdateSubjectDto } from './dto/update-subject.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Subject } from './entities/subject.entity';

@Injectable()
export class SubjectService {
  constructor(
    @InjectRepository(Subject)
    private subjectRepository: Repository<Subject>,
  ) {}

  create(createSubjectDto: CreateSubjectDto) {
    return this.subjectRepository.save(createSubjectDto);
  }

  findAll() {
    return this.subjectRepository.find();
  }

  findOne(id: number) {
    return this.subjectRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateSubjectDto: UpdateSubjectDto) {
    try {
      const updatedSubject = await this.subjectRepository.save({
        id,
        ...updateSubjectDto,
      });
      return updatedSubject;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const subject = await this.subjectRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedSubject = await this.subjectRepository.remove(subject);
      return deletedSubject;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
