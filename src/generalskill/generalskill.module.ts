import { Module } from '@nestjs/common';
import { GeneralskillService } from './generalskill.service';
import { GeneralskillController } from './generalskill.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Generalskill } from './entities/generalskill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Generalskill])],
  controllers: [GeneralskillController],
  providers: [GeneralskillService],
})
export class GeneralskillModule {}
