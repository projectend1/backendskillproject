import { Test, TestingModule } from '@nestjs/testing';
import { GeneralskillService } from './generalskill.service';

describe('GeneralskillService', () => {
  let service: GeneralskillService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeneralskillService],
    }).compile();

    service = module.get<GeneralskillService>(GeneralskillService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
