import { IsNotEmpty } from 'class-validator';

export class CreateGeneralskillDto {
  @IsNotEmpty()
  namegeneral: string;

  @IsNotEmpty()
  generaldescription: string;
}
