import { PartialType } from '@nestjs/mapped-types';
import { CreateGeneralskillDto } from './create-generalskill.dto';

export class UpdateGeneralskillDto extends PartialType(CreateGeneralskillDto) {}
