import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Generalskill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  namegeneral: string;

  @Column()
  generaldescription: string;
}
