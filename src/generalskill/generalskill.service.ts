import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateGeneralskillDto } from './dto/create-generalskill.dto';
import { UpdateGeneralskillDto } from './dto/update-generalskill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Generalskill } from './entities/generalskill.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GeneralskillService {
  constructor(
    @InjectRepository(Generalskill)
    private generalskillRepository: Repository<Generalskill>,
  ) {}

  create(createGeneralskillDto: CreateGeneralskillDto) {
    return this.generalskillRepository.save(createGeneralskillDto);
  }

  findAll() {
    return this.generalskillRepository.find();
  }

  findOne(id: number) {
    return this.generalskillRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateGeneralskillDto: UpdateGeneralskillDto) {
    try {
      const updatedGeneralskill = await this.generalskillRepository.save({
        id,
        ...updateGeneralskillDto,
      });
      return updatedGeneralskill;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const generalskill = await this.generalskillRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedGeneralskill =
        await this.generalskillRepository.remove(generalskill);
      return deletedGeneralskill;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
