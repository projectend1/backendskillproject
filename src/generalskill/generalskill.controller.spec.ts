import { Test, TestingModule } from '@nestjs/testing';
import { GeneralskillController } from './generalskill.controller';
import { GeneralskillService } from './generalskill.service';

describe('GeneralskillController', () => {
  let controller: GeneralskillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GeneralskillController],
      providers: [GeneralskillService],
    }).compile();

    controller = module.get<GeneralskillController>(GeneralskillController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
