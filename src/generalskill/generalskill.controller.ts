import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { GeneralskillService } from './generalskill.service';
import { CreateGeneralskillDto } from './dto/create-generalskill.dto';
import { UpdateGeneralskillDto } from './dto/update-generalskill.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('generalskill')
@UseGuards(JwtAuthGuard)
export class GeneralskillController {
  constructor(private readonly generalskillService: GeneralskillService) {}

  @Post()
  create(@Body() createGeneralskillDto: CreateGeneralskillDto) {
    return this.generalskillService.create(createGeneralskillDto);
  }

  @Get()
  findAll() {
    return this.generalskillService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.generalskillService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateGeneralskillDto: UpdateGeneralskillDto,
  ) {
    return this.generalskillService.update(+id, updateGeneralskillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.generalskillService.remove(+id);
  }
}
