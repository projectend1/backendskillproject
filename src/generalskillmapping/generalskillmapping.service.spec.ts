import { Test, TestingModule } from '@nestjs/testing';
import { GeneralskillmappingService } from './generalskillmapping.service';

describe('GeneralskillmappingService', () => {
  let service: GeneralskillmappingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeneralskillmappingService],
    }).compile();

    service = module.get<GeneralskillmappingService>(
      GeneralskillmappingService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
