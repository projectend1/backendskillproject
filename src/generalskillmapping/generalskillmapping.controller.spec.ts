import { Test, TestingModule } from '@nestjs/testing';
import { GeneralskillmappingController } from './generalskillmapping.controller';
import { GeneralskillmappingService } from './generalskillmapping.service';

describe('GeneralskillmappingController', () => {
  let controller: GeneralskillmappingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GeneralskillmappingController],
      providers: [GeneralskillmappingService],
    }).compile();

    controller = module.get<GeneralskillmappingController>(
      GeneralskillmappingController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
