import { IsNotEmpty, IsNumber } from 'class-validator';
export class CreateGeneralskillmappingDto {
  @IsNotEmpty()
  ploIdgen: number;

  @IsNotEmpty()
  @IsNumber()
  subjectId: number;

  @IsNotEmpty()
  generalskill: number;

  @IsNotEmpty()
  expectLevel: number;
}
