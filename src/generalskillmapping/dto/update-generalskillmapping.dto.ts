import { PartialType } from '@nestjs/mapped-types';
import { CreateGeneralskillmappingDto } from './create-generalskillmapping.dto';

export class UpdateGeneralskillmappingDto extends PartialType(
  CreateGeneralskillmappingDto,
) {}
