import { Module } from '@nestjs/common';
import { GeneralskillmappingService } from './generalskillmapping.service';
import { GeneralskillmappingController } from './generalskillmapping.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Generalskillmapping } from './entities/generalskillmapping.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Generalskillmapping])],
  controllers: [GeneralskillmappingController],
  providers: [GeneralskillmappingService],
})
export class GeneralskillmappingModule {}
