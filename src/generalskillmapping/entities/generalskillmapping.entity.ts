import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Generalskillmapping {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ploIdgen: number;

  @Column()
  subjectId: number;

  @Column()
  generalskill: number;

  @Column()
  expectLevel: number;
}
