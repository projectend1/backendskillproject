import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { GeneralskillmappingService } from './generalskillmapping.service';
import { CreateGeneralskillmappingDto } from './dto/create-generalskillmapping.dto';
import { UpdateGeneralskillmappingDto } from './dto/update-generalskillmapping.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('generalskillmapping')
@UseGuards(JwtAuthGuard)
export class GeneralskillmappingController {
  constructor(
    private readonly generalskillmappingService: GeneralskillmappingService,
  ) {}

  @Post()
  create(@Body() createGeneralskillmappingDto: CreateGeneralskillmappingDto) {
    return this.generalskillmappingService.create(createGeneralskillmappingDto);
  }

  @Get()
  findAll() {
    return this.generalskillmappingService.findAll();
  }

  @Get('/getbysubjectId/:id')
  getbysubjectId(@Param('id') subjectId: string) {
    return this.generalskillmappingService.getbysubjectId(+subjectId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.generalskillmappingService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateGeneralskillmappingDto: UpdateGeneralskillmappingDto,
  ) {
    return this.generalskillmappingService.update(
      +id,
      updateGeneralskillmappingDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.generalskillmappingService.remove(+id);
  }
}
