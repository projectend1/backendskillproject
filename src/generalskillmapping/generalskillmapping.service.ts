import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateGeneralskillmappingDto } from './dto/create-generalskillmapping.dto';
import { UpdateGeneralskillmappingDto } from './dto/update-generalskillmapping.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Generalskillmapping } from './entities/generalskillmapping.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GeneralskillmappingService {
  constructor(
    @InjectRepository(Generalskillmapping)
    private generalskillmappingRepository: Repository<Generalskillmapping>,
  ) {}

  create(createGeneralskillmappingDto: CreateGeneralskillmappingDto) {
    return this.generalskillmappingRepository.save(
      createGeneralskillmappingDto,
    );
  }

  findAll() {
    return this.generalskillmappingRepository.query(
      'SELECT gsp.id, pl.PLO_id , sb.course , sb.nameTh , gs.namegeneral , el.expectLevel FROM generalskillmapping gsp INNER JOIN plo pl ON gsp.ploIdgen = pl.Id INNER JOIN subject sb ON gsp.subjectid = sb.Id INNER JOIN generalskill gs ON gsp.generalskill = gs.Id INNER JOIN expectLevel el ON gsp.expectLevel = el.id',
    );
  }
  getbysubjectId(subjectId: number) {
    return this.generalskillmappingRepository.query(
      'SELECT gsp.id, pl.PLO_id , sb.course , sb.nameTh , gs.namegeneral , el.expectLevel FROM generalskillmapping gsp INNER JOIN plo pl ON gsp.ploIdgen = pl.Id INNER JOIN subject sb ON gsp.subjectid = sb.Id INNER JOIN generalskill gs ON gsp.generalskill = gs.Id INNER JOIN expectLevel el ON gsp.expectLevel = el.id WHERE sb.course = ?',
      [subjectId],
    );
  }

  findOne(id: number) {
    return this.generalskillmappingRepository.findOne({ where: { id: id } });
  }

  async update(
    id: number,
    updateGeneralskillmappingDto: UpdateGeneralskillmappingDto,
  ) {
    try {
      const updatedGeneralskillmapping =
        await this.generalskillmappingRepository.save({
          id,
          ...updateGeneralskillmappingDto,
        });
      return updatedGeneralskillmapping;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const generalskillmapping =
      await this.generalskillmappingRepository.findOne({
        where: { id: id },
      });
    try {
      const deletedGeneralskillmapping =
        await this.generalskillmappingRepository.remove(generalskillmapping);
      return deletedGeneralskillmapping;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
