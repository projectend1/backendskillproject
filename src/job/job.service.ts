import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from './entities/job.entity';
import { Repository } from 'typeorm';

@Injectable()
export class JobService {
  constructor(
    @InjectRepository(Job)
    private jobRepository: Repository<Job>,
  ) {}

  create(createJobDto: CreateJobDto) {
    return this.jobRepository.save(createJobDto);
  }

  findAll() {
    return this.jobRepository.find();
  }

  findOne(id: number) {
    return this.jobRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateJobDto: UpdateJobDto) {
    try {
      const updatedJob = await this.jobRepository.save({
        id,
        ...updateJobDto,
      });
      return updatedJob;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const job = await this.jobRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedJob = await this.jobRepository.remove(job);
      return deletedJob;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
