import { IsNotEmpty } from 'class-validator';

export class CreateJobDto {
  @IsNotEmpty()
  nameEng: string;

  @IsNotEmpty()
  nameTh: string;

  @IsNotEmpty()
  descriptionEng: string;

  @IsNotEmpty()
  descriptionTh: string;
}
