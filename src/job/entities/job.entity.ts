import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Job {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nameEng: string;

  @Column()
  nameTh: string;

  @Column()
  descriptionEng: string;

  @Column()
  descriptionTh: string;
}
