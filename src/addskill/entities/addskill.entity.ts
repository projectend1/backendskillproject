import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Addskill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  subjectId: number;

  @Column()
  mainskillmapping: number;

  @Column()
  generalskillmapping: number;

  @Column()
  year: string;

  @Column()
  studentId: string;

  @Column()
  alllevel: number;

  @Column()
  genlevel: string;
}
