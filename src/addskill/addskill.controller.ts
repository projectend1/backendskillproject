import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AddskillService } from './addskill.service';
import { CreateAddskillDto } from './dto/create-addskill.dto';
import { UpdateAddskillDto } from './dto/update-addskill.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('addskill')
@UseGuards(JwtAuthGuard)
export class AddskillController {
  constructor(private readonly addskillService: AddskillService) {}

  @Post()
  create(@Body() createAddskillDto: CreateAddskillDto[]) {
    return this.addskillService.create(createAddskillDto);
  }

  @Get()
  findAll() {
    return this.addskillService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.addskillService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAddskillDto: UpdateAddskillDto,
  ) {
    return this.addskillService.update(+id, updateAddskillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.addskillService.remove(+id);
  }
}
