import { Test, TestingModule } from '@nestjs/testing';
import { AddskillController } from './addskill.controller';
import { AddskillService } from './addskill.service';

describe('AddskillController', () => {
  let controller: AddskillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AddskillController],
      providers: [AddskillService],
    }).compile();

    controller = module.get<AddskillController>(AddskillController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
