import { IsNotEmpty } from 'class-validator';

export class CreateAddskillDto {
  id: number;

  @IsNotEmpty()
  subjectId: number;

  mainskillmapping: number;

  generalskillmapping: number;

  @IsNotEmpty()
  year: string;

  @IsNotEmpty()
  studentId: string;

  @IsNotEmpty()
  alllevel: number;

  @IsNotEmpty()
  genlevel: string;
}
