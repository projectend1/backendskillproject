import { PartialType } from '@nestjs/mapped-types';
import { CreateAddskillDto } from './create-addskill.dto';

export class UpdateAddskillDto extends PartialType(CreateAddskillDto) {}
