import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAddskillDto } from './dto/create-addskill.dto';
import { UpdateAddskillDto } from './dto/update-addskill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Addskill } from './entities/addskill.entity';

@Injectable()
export class AddskillService {
  subjectRepository: any;
  constructor(
    @InjectRepository(Addskill)
    private addskillRepository: Repository<Addskill>,
  ) {}

  create(createAddskillDto: CreateAddskillDto[]) {
    return this.addskillRepository.save(createAddskillDto);
  }

  findAll() {
    return this.addskillRepository.query(
      'SELECT ast.idstudent, ast.name, ast.lastname FROM addstudent ast LEFT JOIN addskill ask ON ast.Id = ask.studentId ',
    );
  }
  findOne(id: number) {
    return this.addskillRepository.findOne({ where: { id: id } });
  }

  findAllSubjectName() {
    return this.addskillRepository.query('SELECT subject.nameTh FROM subject');
  }

  findOneMainSkill(subjectId: number) {
    return this.addskillRepository.query(
      'SELECT mainskillId FROM mainskillmapping WHERE subject_id = ?',
      [subjectId],
    );
  }

  findOneGeneralSkill(subjectId: number) {
    return this.addskillRepository.query(
      'SELECT generalskill FROM generalskillmapping WHERE subject_id = ?',
      [subjectId],
    );
  }

  // findAllYear(year: number) {
  //   return this.addskillRepository.query(
  //     'SELECT year FROM addskill WHERE year = ?',
  //     [year],
  //   );
  // }

  async update(id: number, updateAddskillDto: UpdateAddskillDto) {
    try {
      const updatedAddskillDto = await this.addskillRepository.save({
        id,
        ...updateAddskillDto,
      });
      return updatedAddskillDto;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const addskill = await this.addskillRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedAddskill = await this.addskillRepository.remove(addskill);
      return deletedAddskill;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
