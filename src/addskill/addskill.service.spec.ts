import { Test, TestingModule } from '@nestjs/testing';
import { AddskillService } from './addskill.service';

describe('AddskillService', () => {
  let service: AddskillService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AddskillService],
    }).compile();

    service = module.get<AddskillService>(AddskillService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
