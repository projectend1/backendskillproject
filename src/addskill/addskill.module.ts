import { Module } from '@nestjs/common';
import { AddskillService } from './addskill.service';
import { AddskillController } from './addskill.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Addskill } from './entities/addskill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Addskill])],
  controllers: [AddskillController],
  providers: [AddskillService],
})
export class AddskillModule {}
