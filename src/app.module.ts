import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { JobModule } from './job/job.module';
import { SubjectModule } from './subject/subject.module';
import { Subject } from './subject/entities/subject.entity';
import { Job } from './job/entities/job.entity';
import { AddstudentModule } from './addstudent/addstudent.module';
import { Addstudent } from './addstudent/entities/addstudent.entity';
import { PloModule } from './plo/plo.module';
import { Plo } from './plo/entities/plo.entity';
import { MainskillModule } from './mainskill/mainskill.module';
import { GeneralskillModule } from './generalskill/generalskill.module';
import { Mainskill } from './mainskill/entities/mainskill.entity';
import { Generalskill } from './generalskill/entities/generalskill.entity';
import { MainskillmappingModule } from './mainskillmapping/mainskillmapping.module';
import { GeneralskillmappingModule } from './generalskillmapping/generalskillmapping.module';
import { AddskillModule } from './addskill/addskill.module';
import { Addskill } from './addskill/entities/addskill.entity';
import { Result } from './result/entities/result.entity';
import { ResultModule } from './result/result.module';
import { Certificate } from './certificates/entities/certificate.entity';
import { Generalskillmapping } from './generalskillmapping/entities/generalskillmapping.entity';
import { Mainskillmapping } from './mainskillmapping/entities/mainskillmapping.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'information .sqlite',
      entities: [
        User,
        Subject,
        Job,
        Addstudent,
        Plo,
        Mainskill,
        Generalskill,
        Addskill,
        Result,
        Certificate,
        Generalskillmapping,
        Mainskillmapping,
        // MainskillmappingModule,
        // GeneralskillmappingModule,
        // Skill,
      ],
      synchronize: true,
    }),
    UsersModule,
    AuthModule,
    JobModule,
    SubjectModule,
    AddstudentModule,
    PloModule,
    MainskillModule,
    GeneralskillModule,
    MainskillmappingModule,
    GeneralskillmappingModule,
    AddskillModule,
    ResultModule,
    // SkillModule,
    // InformationModule,
    // SummarizeModule,
    // ProfileModule,
    // CertificatesModule,
    // SelectsubjectModule,
    // CertificatesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
