import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateResultDto } from './dto/create-result.dto';
import { UpdateResultDto } from './dto/update-result.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Result } from './entities/result.entity';

@Injectable()
export class ResultService {
  constructor(
    @InjectRepository(Result)
    private resultRepository: Repository<Result>,
  ) {}

  create(createResultDto: CreateResultDto) {
    return this.resultRepository.save(createResultDto);
  }

  findAll() {
    return this.resultRepository.query(
      'SELECT ask.id, sb.course , asd.idstudent , mp.namemainEng  ,ask.alllevel , gp.namegeneral, ask.genlevel FROM addskill ask LEFT JOIN subject sb ON ask.subjectId = sb.Id LEFT JOIN mainskill mp ON ask.mainskillmapping = mp.Id LEFT JOIN generalskill gp ON ask.generalskillmapping = gp.Id LEFT JOIN addstudent asd ON ask.studentId = asd.Id',
    );
  }

  findAllStudent(studentId: string) {
    return this.resultRepository.query(
      'SELECT ask.id, sb.course , asd.idstudent , mp.namemainEng  ,ask.alllevel , gp.namegeneral, ask.genlevel FROM addskill ask LEFT JOIN subject sb ON ask.subjectId = sb.Id LEFT JOIN mainskill mp ON ask.mainskillmapping = mp.Id LEFT JOIN generalskill gp ON ask.generalskillmapping = gp.Id LEFT JOIN addstudent asd ON ask.studentId = asd.Id WHERE asd.idstudent = ?',
      [studentId],
    );
  }
  findAllTeacher(studentId: string) {
    return this.resultRepository.query(
      'SELECT ask.id, sb.course , asd.idstudent , mp.namemainEng  ,ask.alllevel , gp.namegeneral, ask.genlevel FROM addskill ask LEFT JOIN subject sb ON ask.subjectId = sb.Id LEFT JOIN mainskill mp ON ask.mainskillmapping = mp.Id LEFT JOIN generalskill gp ON ask.generalskillmapping = gp.Id LEFT JOIN addstudent asd ON ask.studentId = asd.Id WHERE asd.idstudent = ?',
      [studentId],
    );
  }

  findOne(id: number) {
    return this.resultRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateResultDto: UpdateResultDto) {
    try {
      const updatedResult = await this.resultRepository.save({
        id,
        ...updateResultDto,
      });
      return updatedResult;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number) {
    const result = await this.resultRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedResult = await this.resultRepository.remove(result);
      return deletedResult;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
