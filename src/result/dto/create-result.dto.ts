import { IsNotEmpty } from 'class-validator';

export class CreateResultDto {
  id: number;

  @IsNotEmpty()
  subjectId: number;

  namemainEng: number;

  namegeneralEng: number;

  @IsNotEmpty()
  alllevel: number;

  user: string;

  @IsNotEmpty()
  studentId: string;
}
