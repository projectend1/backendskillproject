import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Result {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  subjectId: number;

  @Column()
  namemainEng: number;

  @Column()
  namegeneralEng: number;

  @Column()
  alllevel: number;

  @Column()
  user: string;

  @Column()
  studentId: string;
}
