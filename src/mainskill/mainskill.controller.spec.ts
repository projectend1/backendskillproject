import { Test, TestingModule } from '@nestjs/testing';
import { MainskillController } from './mainskill.controller';
import { MainskillService } from './mainskill.service';

describe('MainskillController', () => {
  let controller: MainskillController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MainskillController],
      providers: [MainskillService],
    }).compile();

    controller = module.get<MainskillController>(MainskillController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
