import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { MainskillService } from './mainskill.service';
import { CreateMainskillDto } from './dto/create-mainskill.dto';
import { UpdateMainskillDto } from './dto/update-mainskill.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('mainskill')
@UseGuards(JwtAuthGuard)
export class MainskillController {
  constructor(private readonly mainskillService: MainskillService) {}

  @Post()
  create(@Body() createMainskillDto: CreateMainskillDto) {
    return this.mainskillService.create(createMainskillDto);
  }

  @Get()
  findAll() {
    return this.mainskillService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.mainskillService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMainskillDto: UpdateMainskillDto,
  ) {
    return this.mainskillService.update(+id, updateMainskillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.mainskillService.remove(+id);
  }
}
