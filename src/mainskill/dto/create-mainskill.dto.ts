import { IsNotEmpty } from 'class-validator';

export class CreateMainskillDto {
  @IsNotEmpty()
  namemainEng: string;

  @IsNotEmpty()
  namemainTh: string;

  @IsNotEmpty()
  descriptionEng: string;

  @IsNotEmpty()
  descriptionTh: string;
}
