import { PartialType } from '@nestjs/mapped-types';
import { CreateMainskillDto } from './create-mainskill.dto';

export class UpdateMainskillDto extends PartialType(CreateMainskillDto) {}
