import { Test, TestingModule } from '@nestjs/testing';
import { MainskillService } from './mainskill.service';

describe('MainskillService', () => {
  let service: MainskillService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MainskillService],
    }).compile();

    service = module.get<MainskillService>(MainskillService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
