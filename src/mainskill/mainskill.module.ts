import { Module } from '@nestjs/common';
import { MainskillService } from './mainskill.service';
import { MainskillController } from './mainskill.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mainskill } from './entities/mainskill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Mainskill])],
  controllers: [MainskillController],
  providers: [MainskillService],
})
export class MainskillModule {}
