import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMainskillDto } from './dto/create-mainskill.dto';
import { UpdateMainskillDto } from './dto/update-mainskill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Mainskill } from './entities/mainskill.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MainskillService {
  constructor(
    @InjectRepository(Mainskill)
    private mainskillRepository: Repository<Mainskill>,
  ) {}

  create(createMainskillDto: CreateMainskillDto) {
    return this.mainskillRepository.save(createMainskillDto);
  }

  findAll() {
    return this.mainskillRepository.find();
  }

  findOne(id: number) {
    return this.mainskillRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateMainskillDto: UpdateMainskillDto) {
    try {
      const updatedMainskill = await this.mainskillRepository.save({
        id,
        ...updateMainskillDto,
      });
      return updatedMainskill;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const mainskill = await this.mainskillRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedMainskill = await this.mainskillRepository.remove(mainskill);
      return deletedMainskill;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
