import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Mainskill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  namemainEng: string;

  @Column()
  namemainTh: string;

  @Column()
  descriptionEng: string;

  @Column()
  descriptionTh: string;
}
